<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <title>Baka-Updates Manga - &#8730;Chuugakusei</title>
  <meta name="description" content="Latest And Newest Manga Release Updates and News.">
  <meta name="keywords" content="manga, anime, updates, update, releases, release, new, scanlators, scanlator, wiki">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="author" content="Manick(Code&amp;Html)JYT(Design)">
  <link rel="shortcut icon" href="favicon.ico">
  <link rel="stylesheet" href="global/style.css" type="text/css">
  <link rel="stylesheet" href="/css/series.css" type="text/css">
<link rel="stylesheet" href="/css/series.css" type="text/css">
<script type="text/javascript" src="/js/system.js"></script>
<script type="text/javascript" src="/js/series_categories_links.js"></script>
<script type="text/javascript" src="/js/lists.js"></script>
<script type="text/javascript" src="/js/series_comments.js"></script>
<script type="text/javascript" src="/js/global.js"></script>

  <link rel="alternate" title="Baka Updates Manga RSS" href="http://www.mangaupdates.com/rss.php" type="application/rss+xml">
 </head>
 <body >
 
  <div id="centered">
   <table cellspacing="0" cellpadding="0" class="content">

    <!-- Start:Banner -->
    <tr>
     <td id="banner"><img src="images/banner_top919.jpg" alt="banner_jpg"></td>
    </tr>
    <!-- End:Banner -->

    <!-- Start:Login Bar -->
    <tr>
     <td>
      <table cellspacing="0" cellpadding="0" id="login">
       <tr>
        <td><img src="images/banner_bottom_919.jpg" alt="manga04_jpg"></td>
        <td><img src="images/login_tab_left.jpg" alt="login_tab_left_jpg"></td>
        <td id="login_box">


         <!-- Start:Login Content -->
         <form action="login.html" method="post">
         <input type="hidden" name="act" value="login">
          <div class="small_bold" id="login_box_padding">Username:&nbsp;<input type="text" class="small_input" size="12" name="username">&nbsp;&nbsp;Password:&nbsp;<input type="password" class="small_input" size="12" name="password">&nbsp;&nbsp;<input type="image" src="images/login.gif" alt="Login">&nbsp;<a href="signup.html?act=forgotpass"><img src="images/question.gif" alt="Forgot Password?"></a></div>
         </form>
         <!-- End:Login Content -->

        </td>
       </tr>
      </table>
     </td>
    </tr>
    <!-- End:Login Bar -->

    <!-- Start:Main Content Area -->
    <tr>
     <td class="content">
      <table cellspacing="0" cellpadding="0" class="content" border="0">
       <tr>
        <!-- Start:Left Bar Content -->
        <td class="left_content">
         <table cellspacing="0" cellpadding="0" class="content">
          <tr><td id="left_row1" class="textbold"><div id="left_title1" class="medium_bold">Submit News</div>Have a piece of news which concerns the manga community?  <a href='news.html'><u>Submit News</u></a>.</td></tr>
          <tr><td id="left_row2" class="text"><div id="left_title2" class="medium_bold">Manga Poll</div>

<!-- Start:Poll -->
<form action="http://www.mangaupdates.com/poll.html" method="POST">
<input type="hidden" name="act" value="poll">
<table cellpadding='2' cellspacing='3' border='0' width='100%'>
 <tr>
  <td colspan='2' class='textbold' align='center'>How do you like your manga?</td>
 </tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1720'></td>
 <td class='textbold' width='90%'>Hot and steamy</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1721'></td>
 <td class='textbold' width='90%'>Cold, salty, and/or teary</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1722'></td>
 <td class='textbold' width='90%'>Spicy and exciting</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1723'></td>
 <td class='textbold' width='90%'>Medium with a bittersweet aftertaste</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1724'></td>
 <td class='textbold' width='90%'>Chewy and necessary to ponder over</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1725'></td>
 <td class='textbold' width='90%'>Sweet and heartfelt</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1726'></td>
 <td class='textbold' width='90%'>Quick like a sugar rush</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1727'></td>
 <td class='textbold' width='90%'>Hard and blunt</td>
</tr>
<tr>
 <td class='textbold' width='10%'><input type='radio' name='poll_choice' value='1728'></td>
 <td class='textbold' width='90%'>Bland and typical</td>
</tr>
<tr vAlign='middle'>
 <td colspan='2' align='center'><input type='image' style='vertical-align: middle;' src='images/vote.gif'>&nbsp;<a href='http://www.mangaupdates.com/poll.html?act=results'><img border='0' src='images/results.gif' style='vertical-align: middle;' alt=''></a></td>
</tr>
<tr vAlign='middle'><td class='text' colspan='2'><center><a href='poll.html?act=old_polls'><u>See Old Polls</u></a></center></td></tr>
</table>
</form>
<!-- End:Poll -->
		   </td>
          </tr>
          <tr><td id="left_row3" class="textsmall">
		  <img src='images/mascot.gif' alt=''/>
		  <!-- chu chu --><br><b>Manga</b> is the Japanese equivalent of comics<br>with a unique style and following.  Join the revolution!  Read some manga today!
		  <br><br>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBgGFetcCxUsnHkHh005Pz3TTTjdaZu3+20FDtYTPaSsu6ZE4aOvP5PVCid+1qi+QfovS+CDma2JVkZ1mZYCWCBt1dIPujpQeDpMrXzfpP5a5nyruAAtitlh6TRKWuLxED15twA8yfZXvFcZvAdSe+I0MXrKmnclsAVzYkeq3TyqjELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIYrUseeiIBOiAgYgkjQV61838HMqvREx9VhyqO07mjAHxfm+ew4t1map/Hkk9nHRuskLMbqmZ/dUhNJsWyphi9jU/eNccpw/51NLJZoQmTbCTDboDXwovTUVWlu1peijmdLrqTGtuFRNTOHXZUNBZa3vQy50JvIcQdDsMdUiVPXIpn61Y6rx8QUi788Wykonoh3yNoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTEwMjA5MDEzNTUxWjAjBgkqhkiG9w0BCQQxFgQUhHH6DB0gYzAPnSb11OstCP8ibj0wDQYJKoZIhvcNAQEBBQAEgYBgeuBkqKmdOWlb63kci3BBmGRlniVwa0HYnZgGgdMnZn9I2Ygnp9laUof0hzs92idMoIKQiQAMEoSMnhON/ARnHdPFjnw/bC/Cw6C7UJRFabpO3WyEL9dZGbYYfJ+RXcRYnanZmNVjn+q0hy138udkzN+Qltp1vFRWxzKvYhmH6A==-----END PKCS7-----
">
<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
		  <br><br>Coded in <a target="_blank" href="http://www.context.cx"><b>ConTEXT</b></a><br><br><b>Join <a href='irc://irc.irchighway.net/baka-updates'>#baka-updates @irc.irchighway.net</a><BR><BR><a href='http://www.mangaupdates.com/rss.php'><U>RSS Feed</U></a><br>&nbsp;<br></b>
		  </td></tr>
         </table>
        </td>
        <!-- End:Left Bar Content -->

        <!-- Start:Center Content -->
        <td class="center_content">
         <table cellspacing="0" cellpadding="0" class="content" border="0">
          <tr>
           <td id="center_row1">
		    <div id="center_row1_content">
             <table id="maintitle" cellspacing="0" cellpadding="0">
              <tr>
               <td width="11"><img src="images/center_tab_left.gif" height="31" width="11" alt="center_left_tab"></td>
               <td class="large_bold" id="page_title">Manga Info</td>
               <td width="11"><img src="images/center_tab_right.gif" height="31" width="15" alt="center_right_tab"></td>
              </tr>
             </table>			     <div id="signupbutton"><a href='http://www.mangaupdates.com/signup.html'><img border='0' src='images/signup.gif' style='vertical-align:middle;' alt=''></a></div>
			</div>
           </td>
          </tr>

          <tr>
           <td id="main_content" class="text">

<table width='100%' cellpadding='0' cellspacing='0' border='0'>

 <tr vAlign='top'>
  <td colspan='2' align='center' class='specialtext'>&nbsp;</td>
 </tr>
 <tr vAlign='middle'>
  <td colspan='2' align='center' class='specialtext' style="background-image:url('http://www.mangaupdates.com//images/letters_bg.gif')" height='24'>ALL&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=A'>A</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=B'>B</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=C'>C</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=D'>D</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=E'>E</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=F'>F</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=G'>G</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=H'>H</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=I'>I</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=J'>J</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=K'>K</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=L'>L</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=M'>M</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=N'>N</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=O'>O</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=P'>P</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=Q'>Q</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=R'>R</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=S'>S</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=T'>T</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=U'>U</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=V'>V</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=W'>W</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=X'>X</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=Y'>Y</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href='http://www.mangaupdates.com/series.html?letter=Z'>Z</a>&nbsp;&nbsp;&nbsp;</td>
 </tr>
</table>
            <table cellpadding='0' cellspacing='0' border='0' width='100%' class='series_content_table'>
              <tr vAlign='top'>
                <td class='text series_content_cell'>
<!-- Start:Series Info-->
<div style="padding: 9px">
<div style="padding: 9px;">
 <span class="releasestitle tabletitle">&#8730;Chuugakusei</span>&nbsp;&nbsp;<span class="text"></span>
</div>
<div id="listContainer">
Login to add items to your list, <b>keep track</b> of your progress, and rate series!
</div>
 <div class="sContainer" text>
  <div class="sMember">
<div class="sCat"><b>Description</b></div>
<div class="sContent" style="text-align:justify">N/A
</div>
<br>
<div class="sCat"><b>Type</b></div>
<div class="sContent" >Manga
</div>
<br>
<div class="sCat"><b>Related Series</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Associated Names</b></div>
<div class="sContent" >&#12523;&#12540;&#12488; &#20013;&#23398;&#29983;<br />&#8730;&#20013;&#23398;&#29983;<br />Chuugakusei<br />Root Junior High School Student<br />Square Root Chuugakusei<br />
</div>
<br>
<div class="sCat"><b>Groups Scanlating</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Latest Release(s)</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Status in Country of Origin</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Completely Scanlated?</b></div>
<div class="sContent" >No
</div>
<br>
<div class="sCat"><b>Anime Start/End Chapter</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>User Reviews</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Forum</b></div>
<div class="sContent" >0 topics, 0 posts<br /><a href='topics.php?fid=66045'><u>Click here to view the forum</u></a>
</div>
<br>
<div class="sCat"><b>User Rating</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Last Updated</b></div>
<div class="sContent" >August 7th 2012, 9:31am PST
</div>
<br>
<div class="sCat"><b>Sponsored Links</b></div>
<div class="sContentAds">

<!-- Manga Updates - 300x250 - Game Advertising Online -->
<iframe marginheight="0" marginwidth="0" scrolling="no" frameborder="0" width="300" height="250" src="http://www3.game-advertising-online.com/index.php?section=serve&id=1339" target="_blank"></iframe><noframes>Game advertisements by <a href="http://www.game-advertising-online.com" target="_blank">Game Advertising Online</a> require iframes.</noframes>
<!-- END GAO 300x250 -->

</div>
<br>

  </div>
 </div>
 <div class="sContainer">
  <div class="sMember">
<div class="sCat"><b>Image</b>&nbsp;[<a href='submit.html?act=add_change&amp;name=inappropriate&amp;id=66525'><u>Report Inappropriate Content</u></a>]</div>
<div class="sContent" ><center><img height='350' width='253' src='http://www.mangaupdates.com/image/i125784.png'></center>
</div>
<br>
<div class="sCat"><b>Genre</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Categories</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Category Recommendations (beta!)</b></div>
<div class="sContent" >N/A (Add some categories, baka!)
</div>
<br>
<div class="sCat"><b>Recommendations</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Author(s)</b></div>
<div class="sContent" ><a href='http://www.mangaupdates.com/authors.html?id=16069' title='Author Info'><u>SAKUYA Tsuitachi</u></a><BR>
</div>
<br>
<div class="sCat"><b>Artist(s)</b></div>
<div class="sContent" ><a href='http://www.mangaupdates.com/authors.html?id=16069' title='Author Info'><u>SAKUYA Tsuitachi</u></a><BR>
</div>
<br>
<div class="sCat"><b>Year</b></div>
<div class="sContent" >2011
</div>
<br>
<div class="sCat"><b>Original Publisher</b></div>
<div class="sContent" ><a href='http://www.mangaupdates.com/publishers.html?id=80' title='Publisher Info'><u>Houbunsha</u></a>
<br />
</div>
<br>
<div class="sCat"><b>Serialized In (magazine)</b></div>
<div class="sContent" ><a href='publishers.html?pubname=Manga+Time+Kirara+Max'><u>Manga Time Kirara Max</u></a> (Houbunsha)
<br />
</div>
<br>
<div class="sCat"><b>Licensed (in English)</b></div>
<div class="sContent" >No
</div>
<br>
<div class="sCat"><b>English Publisher</b></div>
<div class="sContent" >N/A
</div>
<br>
<div class="sCat"><b>Activity Stats</b>&nbsp;(vs. other series)</div>
<div class="sContent" ><a href="stats.html?period=week&amp;series=66525"><u>Weekly</u></a> Pos #<b>1161</b>&nbsp;<img src="images/stat_increase.gif" alt="increased">(+223)<br /><a href="stats.html?period=month1&amp;series=66525"><u>Monthly</u></a> Pos #<b>2385</b>&nbsp;<img src="images/stat_increase.gif" alt="increased">(+829)<br /><a href="stats.html?period=month3&amp;series=66525"><u>3 Month</u></a> Pos #<b>4183</b>&nbsp;<img src="images/stat_increase.gif" alt="increased">(+1519)<br /><a href="stats.html?period=month6&amp;series=66525"><u>6 Month</u></a> Pos #<b>7290</b>&nbsp;<img src="images/stat_increase.gif" alt="increased">(+805)<br />
</div>
<br>
<div class="sCat"><b>List Stats</b></div>
<div class="sContent" ><script type='text/javascript'>
<!--
  function listpopup(list,sid)
  {
    window.open('http://www.mangaupdates.com/series.html?act=list&list=' + list + '&sid=' + sid,'listpopup','width=420,height=400,resizable=no,scrollbars=yes');
  }
//-->
</script>On <b><a href="javascript:listpopup('read', 66525);"><u>5</u></a></b> reading lists<br>On <b><a href="javascript:listpopup('wish', 66525);"><u>16</u></a></b> wish lists<br>On <b>0</b> unfinished lists<br>On <b><a href="javascript:listpopup('custom', 66525);"><u>8</u></a></b> custom lists<br>
</div>
<br>
  </div>
 </div>
</div>
<!-- End:Series Info-->
<div style="width: 100%; float:left"><span class="text"><center><i><b>Note:</b> You must be logged in to update information on this page.</i></center></span><br></div>
                  <table cellpadding='0' cellspacing='0' border='0' width='100%'>
<tr><th class='titlesmall' align='left' style='padding:9px'>Forum Posts</th></tr><tr><td class='text' align='center'>No topics currently in the forum, <a href='topics.php?fid=66045'><u>view the forum</u></a> or <a href='createtopic.php?fid=66045'><u>add a new topic now</u></a>.<br /></td></tr>
	              </table><br><a name='comments'></a><img src='images/tickmark.png' style='display:none' alt='tickmark preload' /><img src='images/xmark.png' style='display:none' alt='xmark preload' /><img src='images/exclamationmark.png' style='display:none' alt='xmark preload' />
                  <table cellpadding='0' cellspacing='0' border='0' width='100%' class='comments_table'>
<tr><td align='left' style='padding:9px'><span class='titlesmall'>User Comments</span>&nbsp;<span class='text'>[ <a href='series.html?id=66525&amp;method=useful#comments'><u>Order by usefulness</u></a> ]</span></td></tr><tr><td style='font-size:6pt'>&nbsp;</td></tr><tr><td><br /><table cellpadding='0' cellspacing='0' border='0' width='100%'>
        <tr>
          <td colspan='2' class='text' align='center'>You must <b><u>login</u></b> to comment for this series! <a href='signup.html'><u>Register</u></a> an account.</td>
        </tr>
</table><br /><table cellpadding='0' cellspacing='0' border='0' width='100%'>
        <tr>
          <td colspan='2' class='text' align='center'>There are no comments/ratings for this series.</td>
        </tr>
</table><div style='padding:12px'>
		

   <table width='100%' align='center' border='0' cellpadding='4' cellspacing='0'>
 	  <tr>
		
	<td align='left' nowrap class='text' style='padding:9px'>
		<form method='post' action='http://www.mangaupdates.com/series.html?id=66525&amp;'>
			Show&nbsp;<select name='perpage' class='inbox'><option value='5' >5</option><option value='10' selected>10</option><option value='15' >15</option><option value='25' >25</option><option value='30' >30</option><option value='40' >40</option><option value='50' >50</option><option value='75' >75</option><option value='100' >100</option></select>&nbsp;entries per page&nbsp;<input type='submit' value='Go' class='button'>
		</form>
	</td>
   	 </tr>
    </table></div></td></tr>
	              </table><hr style='width:90%' /><br><table cellpadding='0' cellspacing='0' border='0' width='100%'>
        <tr>
          <td colspan='2' class='text' align='center'>You must <b><u>login</u></b> to comment for this series! <a href='signup.html'><u>Register</u></a> an account.</td>
        </tr>
</table>
               </td>
              </tr>
             </table>
           </td>
          </tr>

         </table>
        </td>
        <!-- End:Center Content -->

        <!-- Start:Right Bar Content -->
        <td class="right_content">
         <table cellspacing="0" cellpadding="0" class="content">
          <tr><td id="right_row1"><div id="right_title1" class="medium_bold">Manga Search</div>

             <table cellpadding='0' cellspacing='0' border='0' style="width: 100%">
               <tr vAlign='middle'>
                  <td class='textbold search_bar'>
                   <form action='search.html' method='post'>
				    <input type='text' size='11' name='search' value='' class='loginbox'>&nbsp;<input type='image' src='images/go.gif' style='vertical-align: middle;'>
				   </form>
  				  </td>
               </tr>
			</table>
		  </td></tr>
          <tr><td id="right_row2">

<table cellpadding='1' cellspacing='3' border='0' align='center' width='90%'>
 <tr><td class='newsname'><U>MANGA Fu</U></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/index.html'>News</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/whatsnew.html'>What's New!</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/stats.html'>Series Stats</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/forums.php'>Forums</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/chat.html'>Chat</a></td></tr>
 <tr><td height='4'><img src='images/spacer.gif' height='4' alt=''></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/releases.html'>Releases</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/groups.html'>Scanlators</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/series.html?letter=A'>Series Info</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/authors.html'>Mangaka</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/publishers.html'>Publishers</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/reviews.html'>Reviews</a></td></tr>
 <tr><td height='4'><img src='images/spacer.gif' height='4' alt=''></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/genres.html'>Genres</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/categories.html'>Categories</a></td></tr>
 <tr><td height='4'><img src='images/spacer.gif' height='4' alt=''></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/faq.html'>FAQ</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/affiliates.html'>Affiliates</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/members.html'>Members</a></td></tr>
 <tr><td class='nav'><a href='http://www.baka-updates.com/forum/'>BU Forum</a></td></tr>
 <tr><td class='nav'><a href="http://www.baka-updates.com"><span class='text'>Baka-Updates</span></a></td></tr>
 <tr><td height='9'><img src='images/spacer.gif' height='9' alt=''></td></tr>
 <tr><td class='newsname'><U>MEMBERS</U></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/submit.html'>Sign Up</a></td></tr>
 
 <tr><td height='9'><img src='images/spacer.gif' height='9' alt=''></td></tr>
 <tr><td class='newsname'><U>TEAM-BU</U></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/admin.php'>Admin CP</a></td></tr>
 <tr><td class='nav'><a href='http://www.mangaupdates.com/aboutus.html'>About Us</a></td></tr>
</table>
           </td></tr>
          <tr><td id="right_row3" class="text"><br></td></tr>
         </table>
        </td>
        <!-- End:Right Bar Content -->

       </tr>
      </table>
     </td>
    </tr>
    <!-- End:Main Content Area -->
    <!-- Start:Footer -->
    <tr>
     <td>
      <table cellspacing="0" cellpadding="0" border="0">
       <tr>
        <td class="left_content"><br></td>
        <td class="center_content"><img src="images/footer_new.jpg" alt="footer"></td>
        <td class="right_content"><br></td>
       </tr>
      </table>
     </td>
    </tr>
    <!-- End:Footer -->
   </table>
  </div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try{
var pageTracker = _gat._getTracker("UA-177442-2");
pageTracker._trackPageview();
} catch(err) {}
</script>  
 </body>
</html>
