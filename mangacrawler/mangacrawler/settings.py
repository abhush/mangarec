# Scrapy settings for mangacrawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'mangacrawler'

SPIDER_MODULES = ['mangacrawler.spiders']
NEWSPIDER_MODULE = 'mangacrawler.spiders'

ITEM_PIPELINES = ['mangacrawler.pipeline.mangapipeline.SQLitePipeline',
]

EXTENSIONS = {'scrapy.contrib.logstats.LogStats': 500,
}

DOWNLOAD_DELAY = 0.25

LOG_ENABLED = True

#LOG_FILE = 'log.txt'

ROBOTTXT_OBEY = True
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'mangacrawler (+http://www.yourdomain.com)'
