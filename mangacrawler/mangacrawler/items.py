# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class MangaItem(Item):
	#Int
	id = Field()
	#Str
	title = Field()
	#Str 
	mtype = Field()
	#Int
	lrelease = Field()
	#Bool
	complete = Field()
	#Float
	avg = Field()
	#Float
	bavg = Field()
	#[Str]
	genres = Field()
	#[(Str,Int)]
	authors = Field()
	#[(Str,Int)]
	artists = Field()
	#Int
	year = Field()
	#[(Str,Int)]
	opub = Field()
	#[Str]
	mag = Field()
	#[(Str,Int,Int)]
	categories = Field()
	#Int
	nvol = Field()
	#Int
	nchap = Field()
	#Ongoing
	ongoing = Field()
	#Oneshot
	oneshot = Field()
	
	
	
	
