from scrapy.spider import BaseSpider
from mangacrawler.spiders.catspider import CatSpider
from scrapy.selector import HtmlXPathSelector
from mangacrawler.items import MangaItem
import re

def extractListWithId(scontent,pos):
	templ = scontent[pos].select('a/u/text()').extract()
	#Filter the items without id
	templ = filter(lambda x : x.lower()!=u'Add'.lower(),templ)
	itemid = scontent[pos].select('a').re('[0-9]+')
	itemid = map(int,itemid)
	#Get the items ids and merge into tuples
	templ  = [(x,y) for x in templ for y in itemid if templ.index(x)==itemid.index(y)]
	#Get the items without id (always appears with a [add] besides it); create tuples with -1 id and merge into main list
	#Higly Inneficient (double re)
	items = templ + [(re.search('([\w ]+).*\[',x).group(1),-1) for x in scontent[pos].select('text()').re('[\w ]+.*\[')]
	return items

class MangaSpider(BaseSpider):
	name = "manga"
	allowed_domains = ["mangaupdates.com"]
	start_urls = ["http://www.mangaupdates.com/series.html?id=6025"]
	
	def __init__(self,id=0):
		BaseSpider.__init__(self)
		super( MangaSpider, self ).__init__()
		self.id = id;

	def parse(self, response):
		item = MangaItem()
		filename = response.url.split("/")[-2]
		open(filename,'wb').write(response.body)
		hxs = HtmlXPathSelector(response)

		#Title
		scontent = hxs.select('//div[@class="sContent"]')
		if len(scontent) <=1:
			return item
		item['id'] = re.search('.+id=(\d+)',response.url).group(1)
		title = hxs.select('//span[@class="releasestitle tabletitle"]/text()').extract()[0]
		item['title'] = title

		#Manga Type
		mtype = scontent[1].select('//div[@class="sContent"]')[1].select('text()').extract()[0]
		item['mtype'] = mtype.rstrip()

		#Latest Release
		templ = scontent[5].select('span/text()').extract()
		if len(templ)!=0:
			temps = scontent[5].select('span/text()').extract()[0]
			temps = temps.split('d')[0][1:]
			lrelease = int(temps)
		else:
			lrelease = -1
		item['lrelease'] = lrelease

		#Status
		templ = scontent[6].select('text()').extract()
		nchap = -1
		nvol = -1
		oneshot = False
		ongoing = False
		for t in templ:
			if t.lower().find('chapter')!= -1:
				nchap += int(re.search('\d+',t).group(0))
			if t.lower().find('volume')!= -1:
				nchap += int(re.search('\d+',t).group(0))
			if t.lower().find('oneshot')!= -1:
				oneshot = True
				if nchap == -1:
					nchap = 1
				else:
					nchap +=1
			if t.lower().find('ongoing')!= -1:
				ongoing = True
		item['nvol'] = nvol
		item['nchap'] = nchap
		item['oneshot'] = oneshot
		item['ongoing'] = ongoing

		#Completly Scanlated
		temps = scontent[7].select('text()').extract()[0]
		if temps.lower() == "yes":
			complete = True
		else:
			complete = False
		item['complete'] = complete

		#Average & Bayesian Average
		temps = scontent[11].select('text()').extract()[0]
		if temps != u'N/A\n':
			temps = re.search('[0-9.]+',temps).group(0)
			avg = float(temps)
			bavg  = float(scontent[11].select('b/text()').extract()[0])
		else:
			avg = -1
			bavg = -1
		item['avg'] = avg
		item['bavg']= bavg

		#Genres
		genres = scontent[14].select('a/u/text()').extract()
		item['genres'] = genres

		#Author(s)
		item['authors'] = extractListWithId(scontent,18)

		#Artist(s) (same as author extraction)
		item['artists'] = extractListWithId(scontent,19)

		#Year
		try:
			year = int(scontent[20].select('text()').extract()[0])
		except ValueError:
			year = -1
		item['year']= year

		#Original Publisher
		item['opub'] = extractListWithId(scontent,21)

		#Magazine
		templ = scontent[22].select('a/u/text()').extract()
		if len(templ)!=0:
			mag = scontent[22].select('a/u/text()').extract()
		else:
			mag=[]
		item['mag'] = mag

		#Categories
		cats = CatSpider(id)
		item['categories'] = cats.parse(response)
		
		return item
		


		

			

		
	
