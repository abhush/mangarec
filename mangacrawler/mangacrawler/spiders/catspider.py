from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
import re

class CatSpider(BaseSpider):
	name = "categories"
	allowed_domains = ["mangaupdates.com"]
	id = 0
	start_urls = ["http://www.mangaupdates.com/ajax/show_categories.php?s="+str(id)+"&type=1"]
	
	def __init__(self,id=0):
		BaseSpider.__init__(self)
		super( CatSpider, self ).__init__()
		self.id = id;
		

	def parse(self, response):
		filename = response.url.split("/")[-2]
		open(filename,'wb').write(response.body)
		hxs = HtmlXPathSelector(response)
		catnames = hxs.select('//li/a/text()').extract()
		catvotes = hxs.select('//li/a').select('attribute::title').re('(\d+,\d+)')	
		catvotes = [map(int,x.rsplit(',')) for x in catvotes]
		cattuples = []
		for i in range(0,len(catvotes)):
			cattuples.append(tuple([catnames[i]] + catvotes[i]))
		return cattuples
		
	
