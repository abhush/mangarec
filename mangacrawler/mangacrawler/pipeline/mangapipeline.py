# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
import sqlite3 as lite

class SQLitePipeline(object):
	
	db = '../database/manga.db'
	lastauatid = -1
	lastopubid = -1

	TABLEMANGA = 'manga'
	TABLEAUTHOR_ARTIST = 'author_artist'
	TABLECATEGORY = 'category'
	TABLEPUBLISHER = 'publisher'
	TABLEGENRE = 'genre'
	TABLEMAGAZINE = 'magazine'
	con= None
	cur= None
	
	
	def __init__(self):
		try:
			self.con= lite.connect(self.db)
			self.cur= self.con.cursor()
			self.con.execute('pragma foreign_keys = ON');
			self.cur.execute('SELECT MIN(id) from {0}'.format(self.TABLEAUTHOR_ARTIST))
			lastauatid = self.cur.fetchone()[0]
			if lastauatid == None:
				lastauatid=-1
			self.cur.execute('SELECT MIN(id) from {0}'.format(self.TABLEPUBLISHER))
			lastopubid = self.cur.fetchone()[0]
			if lastopubid == None:
				lastopubid = -1
		except lite.Error, e:
			print 'Init error' + e.args[0]
	def __del__(self):	
		if self.con:
			self.con.close()
			
	def insertItemWithId(self,mid,ituple,tablename):
		id = ituple[1]
		if id==-1:
			if tablename == self.TABLEPUBLISHER:
				id = self.lastopubid
				self.lastopubid = self.lastopubid -1
			elif tablename == self.TABLEAUTHOR_ARTIST:
				id = self.lastauatid
				self.lastauatid = self.lastauatid -1
		try:
			self.cur.execute('INSERT INTO {0} VALUES (?,?)'.format(tablename),(id,ituple[0]))
			self.cur.execute('INSERT INTO {0}_{1} VALUES (?,?)'.format(self.TABLEMANGA,tablename),(mid,id))
		except lite.IntegrityError,e:
			print  'iwid ' + tablename + ' ' +  str(ituple) + ' ' + str(id) + ' ' +  e.args[0]
	
	def insertItem(self,mid,item,tablename):
		try:
			self.cur.execute('INSERT INTO {0} VALUES (?)'.format(tablename),(item,))
			self.cur.execute('INSERT INTO {0}_{1} VALUES (?,?)'.format(self.TABLEMANGA,tablename),(mid,item))
		except lite.IntegrityError,e: #Inserting duplicate catch
			print tablename +' '  +  str(item) + ' ' + e.args[0]

	def process_item(self, item, spider):
		try:
			#Populate Null Values
			for value in item.keys():
				if item[value] == -1:
					item[value]=None
				if item[value] == u'':
					item[value]=None
			#Insert into basic info into mangatable
			self.cur.execute('INSERT INTO {0} VALUES (?,?,?,?,?,?,?,?,?,?,?,?)'.format(self.TABLEMANGA), \
			(item['id'],item['title'],item['mtype'],item['lrelease'],item['complete'],item['avg'],item['bavg'],item['nchap'],item['nvol'],item['year'],item['ongoing'],item['oneshot']))
			
			#Insert Categories
			for cat in item['categories']:
				try:
					#print str(cat)
					self.cur.execute('INSERT INTO {0} VALUES (?)'.format(self.TABLECATEGORY) ,(cat[0],))
				except lite.IntegrityError,e: #Inserting duplicate catch
					print 'Categories Problem: ' + e.args[0]
				self.cur.execute('INSERT INTO {0}_{1} VALUES (?,?,?,?)'.format(self.TABLEMANGA,self.TABLECATEGORY),(item['id'],cat[0],cat[1],cat[2]))

			#Insert Genres
			for genre in item['genres']:
				#print genre
				self.insertItem(item['id'],genre,self.TABLEGENRE)
			#Insert Magazine
			for magazine in item['mag']:
				#print magazine
				self.insertItem(item['id'],magazine,self.TABLEMAGAZINE)
			#Insert Authors/Artists
			for artist in item['artists']:
				#print str(artist)
				self.insertItemWithId(item['id'],artist,self.TABLEAUTHOR_ARTIST)
			for author in item['authors']:
				#print str(author)
				self.insertItemWithId(item['id'],author,self.TABLEAUTHOR_ARTIST)
			#Insert Publisher
			for publisher in item['opub']:
				#print str(publisher)
				self.insertItemWithId(item['id'],publisher,self.TABLEPUBLISHER)
			self.con.commit()
		except lite.Error, e:
			print 'DB Error :(' + e.args[0]
		return item
