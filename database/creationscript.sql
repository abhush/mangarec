PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;

CREATE TABLE author_artist(id INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL UNIQUE);
CREATE TABLE manga(id INTEGER NOT NULL PRIMARY KEY, title TEXT, type TEXT, latest_release INTEGER, complete INTEGER, average REAL, bayesian_average REAL, number_chapter INTEGER, number_volume INTEGER, year INTEGER, ongoing INTEGER, oneshot INTEGER);
CREATE TABLE publisher(id INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL UNIQUE);
CREATE TABLE Magazine(name TEXT NOT NULL PRIMARY KEY);
CREATE TABLE Genre(name TEXT NOT NULL PRIMARY KEY) ;
CREATE TABLE Category(name TEXT NOT NULL PRIMARY KEY);

--Many to Many tables
CREATE TABLE manga_author_artist(author_artist_id REFERENCES author_artist(id), manga_id REFERENCES manga(id));
CREATE TABLE manga_publisher(author_artist_id REFERENCES manga(id), manga_id REFERENCES manga(id));
CREATE TABLE manga_genre(manga_id REFERENCES manga(id), genre_name REFERENCES Genre(name));
CREATE TABLE manga_category(manga_id REFERENCES manga(id), category_name REFERENCES Category(name), agree INTEGER, disagree INTEGER, PRIMARY KEY (manga_id,category_name));
CREATE TABLE manga_magazine(manga_id REFERENCES manga(id), magazine_name REFERENCES Magazine(name), PRIMARY KEY (manga_id,magazine_name));
COMMIT;
